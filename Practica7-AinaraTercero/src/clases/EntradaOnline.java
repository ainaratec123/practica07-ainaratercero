package clases;

import java.io.Serializable;
import java.time.LocalDate;

public class EntradaOnline extends Entrada implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String email;
	private String direccion;
	private String descuento;
	private String numeroCuenta;
	//Cantidad de descuento que se hara al precio por comprarlo online
	public static final double CANTIDAD = 2.8;

	public EntradaOnline(String codEntrada, LocalDate fechaConcierto, LocalDate fechaCompra, double precio) {
		super(codEntrada, fechaConcierto, fechaCompra, precio);
		// TODO Auto-generated constructor stub
	}

	
	
	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}



	public String getDireccion() {
		return direccion;
	}


	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}



	public String getDescuento() {
		return descuento;
	}



	public void setDescuento(String descuento) {
		this.descuento = descuento;
	}



	public String getNumeroCuenta() {
		return numeroCuenta;
	}



	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}



	@Override
	public double suplemento() {
		double total =0;
		total = precio-CANTIDAD;
		
		return total;
	}


	@Override
	public String toString() {
		return super.toString() + "EntradaOnline [email=" + email + ", direccion=" + direccion + ", descuento=" + descuento
				+ ", numeroCuenta=" + numeroCuenta + "]";
	}
	
	
	
	
	

}
