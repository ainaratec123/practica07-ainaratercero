package clases;

import java.io.Serializable;
import java.time.LocalDate;

public abstract class Entrada implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	String codEntrada;
	LocalDate fechaConcierto;
	LocalDate fechaCompra;
	double precio;
	
	
	public Entrada(String codEntrada, LocalDate fechaConcierto, LocalDate fechaCompra, double precio) {
		
		this.codEntrada = codEntrada;
		this.fechaConcierto = fechaConcierto;
		this.fechaCompra = fechaCompra;
		this.precio = precio;
	}
	
	public abstract double suplemento() ;

	public String getCodEntrada() {
		return codEntrada;
	}

	public void setCodEntrada(String codEntrada) {
		this.codEntrada = codEntrada;
	}

	public LocalDate getFechaConcierto() {
		return fechaConcierto;
	}

	public void setFechaConcierto(LocalDate fechaConcierto) {
		this.fechaConcierto = fechaConcierto;
	}

	public LocalDate getFechaCompra() {
		return fechaCompra;
	}

	public void setFechaCompra(LocalDate fechaCompra) {
		this.fechaCompra = fechaCompra;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	@Override
	public String toString() {
		return "Entrada [codEntrada=" + codEntrada + ", fechaConcierto=" + fechaConcierto + ", fechaCompra="
				+ fechaCompra + ", precio=" + precio + "]";
	}
	
	
	
	
	
	
	
	
	
	
	

}
