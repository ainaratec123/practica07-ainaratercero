package clases;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;
import java.util.Vector;

public class FicherosSecuenciales {

	
	
	private String archivo;

	//constructor
	
	public FicherosSecuenciales( String archivo) {
		this.archivo = archivo;
		
	}
	
	//etodo para crear archivo
	
	public void crearArchivo() throws IOException {
		System.out.println("CREAR ARCHIVO");
		
		BufferedReader escaner = new BufferedReader(new InputStreamReader(System.in));
		String linea;
	
		PrintWriter fichero = new PrintWriter(new BufferedWriter(new FileWriter(this.archivo)));
		System.out.println("Introducir datos (* para parar)");
		
		linea = escaner.readLine();
		while (!linea.equalsIgnoreCase("*")) {
			fichero.println(linea);
			linea = escaner.readLine();
		}
		System.out.println("El archivo ha sido creado");
		
		fichero.close();

	}
	
	
	//metodo para visualizar
	
	public void visualizarArchivo() throws IOException {
		String linea;
		System.out.println("Visualizando archivo --> " + this.archivo);
		
		BufferedReader fichero = new BufferedReader(new FileReader(this.archivo));
		
		linea = fichero.readLine();
		while (linea != null) {
			System.out.println(linea);
			linea = fichero.readLine();
		}
	
		fichero.close();
	}

	
	//metodo para buscar
	public void buscar() {
	
		String palabraBuscada;
		String linea;
		Scanner escaner = new Scanner(System.in);
		System.out.println("Introduce la palabra que quieres buscar");
		palabraBuscada = escaner.nextLine();
		
		Vector<String> vectorPrueba = new Vector<String>();

		try {
			
			BufferedReader fichero = new BufferedReader(new FileReader(this.archivo));
		
			linea = fichero.readLine();
			while (linea != null) {
				String letra;
				String palabra = "";
				for (int j = 0; j < linea.length(); j++) {
					letra = linea.substring(j, j + 1);
					if (letra.equalsIgnoreCase(" ") == false) {
						palabra = palabra + letra;
						
					} 
				}
				vectorPrueba.add(palabra);
				palabra = "";
				linea = fichero.readLine();
			}
			
			int cp = 0;
			for (int j = 0; j < vectorPrueba.size(); j++) {
				String valor = vectorPrueba.elementAt(j).toString();
				if (valor.equalsIgnoreCase(palabraBuscada) == true) {
					cp++;
					System.out.println("La palabra buscada es: " +palabraBuscada);
				}
			}
			if (cp == 0) {
				System.out.println("La palabra no se encuentra en el archivo");
			} else {
				System.out.println("La palabra se encuentra en el archivo " + cp + " veces");
			}
			
			fichero.close();
		} catch (FileNotFoundException e) {
			System.out.println("El fichero no se ha encontrado");
			System.exit(0);
		} catch (IOException e) {
			System.out.println("Error de entrada");
			System.exit(0);
		}

	}
	
	//metodo para ordenar alfabeticamente
	public void ordenarAlfabeticamente() {
		String linea;
		System.out.println("ORDENAR ALFABETICAMENTE ");
		ArrayList<String> orden = new ArrayList<String>();

		try {
			
			BufferedReader fichero = new BufferedReader(new FileReader(this.archivo));
			
			linea = fichero.readLine();
			while (linea != null) {
				orden.add(linea);
				linea = fichero.readLine();
			}
			
			fichero.close();
			
			PrintWriter archivo = new PrintWriter(new FileWriter(this.archivo));
			
				Collections.sort(orden);
				
				for (int i = 0; i < orden.size(); i++) {
					Collections.sort(orden);
					archivo.println(orden.get(i));
				}
				
		
			archivo.close();
		} catch (FileNotFoundException e) {
			System.out.println("El fichero no se ha encontrado");
			System.exit(0);
		} catch (IOException e) {
			System.out.println("Error de entrada ");
			System.exit(0);
		}

	}
	
	
	//metodo para poner la primera en mayuscula
	public void primeraMayus() {
		String linea;
		System.out.println("PRIMERA LETRA MAYUSCULA");
		ArrayList<String> orden = new ArrayList<String>();

		try {
			
			BufferedReader fichero = new BufferedReader(new FileReader(this.archivo));
			
			linea = fichero.readLine();
			while (linea != null) {
				orden.add(linea);
				linea = fichero.readLine();
			}
			
			fichero.close();
			
			PrintWriter archivo = new PrintWriter(new FileWriter(this.archivo));
			
				
				
				for (int i = 0; i < orden.size(); i++) {
					
					
					
					archivo.println(orden.get(i).substring(0,1).toUpperCase() + orden.get(i).substring(1));
				}
				
				
			
			
			archivo.close();
		} catch (FileNotFoundException e) {
			System.out.println("El fichero no se ha encontrado");
			System.exit(0);
		} catch (IOException e) {
			System.out.println("Error de entrada ");
			System.exit(0);
		}

	}
	
	
	//metodo para numero de  lineas
	public int numeroLineas() {
		System.out.println("CONTADOR DE LINEAS");
		String linea;
		int contador = 0;

		try {
			
			BufferedReader fichero = new BufferedReader(new FileReader(this.archivo));
			
			linea = fichero.readLine();
			while (linea != null) {
				contador++;
				linea = fichero.readLine();
			}
			
			fichero.close();
		} catch (FileNotFoundException e) {
			System.out.println("El fichero no se ha encontrado");
			System.exit(0);
		} catch (IOException e) {
			System.out.println("Error de entrada ");
			System.exit(0);
		}
		return contador;

	}
	
	
	
	//metodo para mostar sin espacios contenido de fichero sin cambiar
	public void mostarSinEspacios() {
		int linea ;
		
		
		System.out.println("MOSTRAR SIN ESPACIOS");
		

		try {
			
			BufferedReader fichero = new BufferedReader(new FileReader(this.archivo));
			
			linea = fichero.read();
			while (linea != -1) {
				
				if (linea!=32){
					System.out.print((char)linea);
				}
				linea=fichero.read();
			}
			
			
			fichero.close();
			
		} catch (FileNotFoundException e) {
			System.out.println("El fichero no se ha encontrado");
			System.exit(0);
		} catch (IOException e) {
			System.out.println("Error de entrada ");
			System.exit(0);
		}
		

	}
	
	
	//metodo para contarconsonantes
	
	public void contarConosantes() {
		int linea ;
		int contador=0;
	
		
		System.out.println("CONTAR CONSONANTES");
		

		try {
			
			BufferedReader fichero = new BufferedReader(new FileReader(this.archivo));
			
			linea = fichero.read();
			while (linea != -1) {
				
				
				if (linea!=65 && linea!=69 && linea!=73 && linea!=79 && linea!=85
						&& linea!=97 && linea!=101 && linea!=105 && linea!=111 && linea!=117){
					contador++;
				}
				linea=fichero.read();
					
				
				
				
				
				
			}
			
			System.out.println(contador);
		
			fichero.close();
			
		} catch (FileNotFoundException e) {
			System.out.println("El fichero no se ha encontrado");
			System.exit(0);
		} catch (IOException e) {
			System.out.println("Error de entrada ");
			System.exit(0);
		}
		
		
	}
	
	
	
		
}
