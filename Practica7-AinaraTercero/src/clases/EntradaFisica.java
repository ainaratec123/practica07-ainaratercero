package clases;

import java.io.Serializable;
import java.time.LocalDate;

public class EntradaFisica extends Entrada implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	  private int  codigoPostal;
	  private int numeroSocio;
	  private String tipoPago;
	  //suplemento que se hara por comprar las entradas fisicamente
	  public static double CANTIDAD = 3.8;

	public EntradaFisica(String codEntrada, LocalDate fechaConcierto, LocalDate fechaCompra, double precio) {
		super(codEntrada, fechaConcierto, fechaCompra, precio);
		// TODO Auto-generated constructor stub
	}

	

	
	public int getCodigoPostal() {
		return codigoPostal;
	}


	public void setCodigoPostal(int codigoPostal) {
		this.codigoPostal = codigoPostal;
	}


	public int getNumeroSocio() {
		return numeroSocio;
	}


	public void setNumeroSocio(int numeroSocio) {
		this.numeroSocio = numeroSocio;
	}



	public String getTipoPago() {
		return tipoPago;
	}


	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago;
	}







	@Override
	public double suplemento() {
		double total=0;
		total = precio+CANTIDAD;
		return total;
	}




	@Override
	public String toString() {
		return super.toString() + "EntradaFisica [codigoPostal=" + codigoPostal + ", numeroSocio=" + numeroSocio + ", tipoPago=" + tipoPago
				+ "]";
	}
	
	
	

}
