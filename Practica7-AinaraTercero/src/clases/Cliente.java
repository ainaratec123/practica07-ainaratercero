package clases;

import java.io.Serializable;


public class Cliente implements Serializable{

	/**
	 * 
	 */
	
	private static final long serialVersionUID = 1L;
	
	
	private String nombre;
	private String apellido;
	private String dni;
	private int telefono;
	
	
	public Cliente(String nombre, String apellido, String dni, int telefono) {
		this.nombre = nombre;
		this.apellido = apellido;
		this.dni = dni;
		this.telefono = telefono;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getApellido() {
		return apellido;
	}


	public void setApellido(String apellido) {
		this.apellido = apellido;
	}


	public String getDni() {
		return dni;
	}


	public void setDni(String dni) {
		this.dni = dni;
	}


	public int getTelefono() {
		return telefono;
	}


	public void setTelefono(int telefono) {
		this.telefono = telefono;
	}


	@Override
	public String toString() {
		return " \n Cliente [nombre=" + nombre + ", apellido=" + apellido + ", dni=" + dni + ", telefono=" + telefono + "] " ;
	}
	
	
	
	
	
	
	
	
	
	

}
