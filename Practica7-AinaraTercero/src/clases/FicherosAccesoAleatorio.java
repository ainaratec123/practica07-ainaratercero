package clases;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;
import java.util.Vector;

import org.omg.Messaging.SyncScopeHelper;

public class FicherosAccesoAleatorio {
	String archivo;
	
	//constructor

	public FicherosAccesoAleatorio(String archivo) {
		this.archivo = archivo;
	}

	
//metodo para rellenar archivo
	public void rellenarArchivo() {
		try {
			BufferedReader fichero = new BufferedReader(new InputStreamReader(System.in));
			RandomAccessFile ficheroAleatorio = new RandomAccessFile(archivo, "rw");
			ficheroAleatorio.seek(ficheroAleatorio.length());
			String respuesta = "";
			do {
				System.out.println("Introduce el dato que quieres introducir en el fichero Aleatorio");
				String dato = fichero.readLine();
				dato = formatearDato(dato, 20);
				ficheroAleatorio.writeUTF(dato);
				System.out.println("¿Deseas añadir otro dato?");
				respuesta = fichero.readLine();
			} while (respuesta.equalsIgnoreCase("si"));
			ficheroAleatorio.close();
		} catch (IOException e) {
			System.out.println("error");
		}
	}
	
	//metodo para formateo de fichero

	private String formatearDato(String dato, int lon) {
		
		if (dato.length() > lon) {
			
			return dato.substring(0, lon);
		} else {
			
			for (int i = dato.length(); i < lon; i++) {
				dato = dato + " ";
			}
		}
		return dato;
	}
	
	
	//metodo visualizar 

	public void visualizarArchivo() throws IOException {
		try {
			RandomAccessFile ficheroAleatorio = new RandomAccessFile(archivo, "rw");
			String dato = "";
			boolean finFichero = false;
			do {
				try {
					dato = ficheroAleatorio.readUTF();
					System.out.println(dato);
				} catch (EOFException e) {
					finFichero = true;
					ficheroAleatorio.close();
				}
			}while(!finFichero);
			
		}catch(IOException e) {
			System.out.println("error");
			
			
		}
	}
	
	//metodo modificar

	public void modificarArchivo() {
		try {
			
			BufferedReader fichero = new BufferedReader(new InputStreamReader(System.in));
			String datoViejo = "Dato viejo";
			String datoNuevo = "Dato nuevo";
			System.out.println("Introduce el dato que quieres modificar: ");
			datoViejo = fichero.readLine();
			System.out.println("Introduce el dato nuevo: ");
			datoNuevo = fichero.readLine();
			
			RandomAccessFile ficheroAleatorio = new RandomAccessFile(archivo, "rw");
			
			String nombre;
			boolean finFichero = false;
			boolean valorEncontrado = false;
			do {
				try {
					nombre = ficheroAleatorio.readUTF();
					if (nombre.trim().equalsIgnoreCase(datoViejo)) {
						
						ficheroAleatorio.seek(ficheroAleatorio.getFilePointer() - 22);
						datoNuevo = formatearDato(datoNuevo, 20);
						ficheroAleatorio.writeUTF(datoNuevo);
						valorEncontrado = true;
					}
				} catch (EOFException e) {
					ficheroAleatorio.close();
					finFichero = true;
				}
			} while (!finFichero);
			if (valorEncontrado == false) {
				System.out.println("El dato no existe en el fichero");
			} else {
				System.out.println("El dato ha sido modificado");
			}
		} catch (IOException e) {
			System.out.println("error");
		}
	}

	
	//metodo comprobar mes
	

	
	public void comprobarMes() {
		try {
			
			BufferedReader fichero = new BufferedReader(new InputStreamReader(System.in));
			String mes;
			boolean finFichero = false;
			String nombre;
			int contador =0;
			System.out.println("Introduce el mes (texto) ");
			mes = fichero.readLine();
		
			RandomAccessFile ficheroAleatorio = new RandomAccessFile(archivo, "rw");
			
				
				try {
					nombre = ficheroAleatorio.readUTF();
					if (nombre.trim().contains(mes)) {
						contador = contador +1;	
						
					}

					if (contador>=1) {
						System.out.println("Si hay compras de entradas en el mes de " + mes);
					}
					else {
						System.out.println("No hay entradas compradas en el mes  de " + mes);
					}
	
				} 
				
				
				catch (EOFException e) 
				{
					ficheroAleatorio.close();
					finFichero = true;
				}
	
			
		} catch (IOException e) {
			System.out.println("error");
		}
		

		
	}
	//metodo para convertir a mayusculas
	public void convertirMayus() {
		String linea;
		System.out.println("CONVERTIR MAYUSCULAS/MINUSCULAS");
		
		
		ArrayList<String> v = new ArrayList<String>();

		try {
			// 1.- conecto en modo lectura
			BufferedReader fuente = new BufferedReader(new FileReader(this.archivo));
			RandomAccessFile ficheroAleatorio = new RandomAccessFile(archivo, "rw");
			
			linea = ficheroAleatorio.readLine();
			while (linea != null) {
				v.add(linea);
				linea = ficheroAleatorio.readLine();
			}
			
			PrintWriter archivo = new PrintWriter(new FileWriter(this.archivo));
			for (int i = 0; i < v.size(); i++) {
				
					archivo.write(v.get(i).toUpperCase());
				
					
				}
			
		
			archivo.close();
		} catch (FileNotFoundException e) {
			System.out.println("Fichero no encontrado");
			System.exit(0);
		} catch (IOException e) {
			System.out.println("Error de entrada salida");
			System.exit(0);
		}

	}
	
	
	//metodo para contarvocales
	public void contarVocales() {
		int linea ;
		int contador=0;
	
		
		System.out.println("CONTAR VOCALES");
		

		try {
			
			BufferedReader fichero = new BufferedReader(new FileReader(this.archivo));
			RandomAccessFile ficheroAleatorio = new RandomAccessFile(archivo, "rw");
			
			linea = ficheroAleatorio.read();
			while (linea != -1) {
				
				
				if (linea!=65 && linea!=69 && linea!=73 && linea!=79 && linea!=85
						&& linea!=97 && linea!=101 && linea!=105 && linea!=111 && linea!=117){
					
				}
				else {
					contador++;
				}
				linea=ficheroAleatorio.read();
					
				
				
				
				
				
			}
			
			System.out.println(contador);
			
		
			
		} catch (FileNotFoundException e) {
			System.out.println("El fichero no se ha encontrado");
			System.exit(0);
		} catch (IOException e) {
			System.out.println("Error de entrada ");
			System.exit(0);
		}
		
		
	}
	
	//metodo para convertir en minusculas
	
	public void convertirMinusculas() {
		String linea;
		System.out.println("CONVERTIR MINUSCULAS");
		
		
		ArrayList<String> v = new ArrayList<String>();

		try {
			
			BufferedReader fuente = new BufferedReader(new FileReader(this.archivo));
			RandomAccessFile ficheroAleatorio = new RandomAccessFile(archivo, "rw");
			
			linea = ficheroAleatorio.readLine();
			while (linea != null) {
				v.add(linea);
				linea = ficheroAleatorio.readLine();
			}
			
			PrintWriter archivo = new PrintWriter(new FileWriter(this.archivo));
			for (int i = 0; i < v.size(); i++) {
				
					archivo.write(v.get(i).toLowerCase());
				
					
				}

			archivo.close();
		} catch (FileNotFoundException e) {
			System.out.println("Fichero no encontrado");
			System.exit(0);
		} catch (IOException e) {
			System.out.println("Error de entrada salida");
			System.exit(0);
		}

	}
	
	//metodo contador de lineas

	public int contadorFechas() {
		System.out.println("CONTADOR DE FECHAS");
		String linea;
		int contador = 0;

		try {
		
			BufferedReader fichero = new BufferedReader(new FileReader(this.archivo));
			RandomAccessFile ficheroAleatorio = new RandomAccessFile(archivo, "rw");
			
			linea = ficheroAleatorio.readLine();
			while (linea != null) {
				contador++;
				linea = ficheroAleatorio.readLine();
			}
	
			fichero.close();
		} catch (FileNotFoundException e) {
			System.out.println("El fichero no se ha encontrado");
			System.exit(0);
		} catch (IOException e) {
			System.out.println("Error de entrada ");
			System.exit(0);
		}
		return contador;

	}
	
	
	
	
}