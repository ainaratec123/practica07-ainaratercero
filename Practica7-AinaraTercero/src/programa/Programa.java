package programa;

import java.io.IOException;
import java.util.Scanner;

import clases.FicherosAccesoAleatorio;
import clases.FicherosSecuenciales;

public class Programa {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		//escaner
		 
		Scanner escaner = new Scanner(System.in);
		
		
		//Intrucion por teclado de archivo para secuencial
		
		System.out.println("Introduce el nombre del archivo");
		String nombreArchivo=escaner.nextLine();
		
		//instancias para los ficheros
	
		FicherosSecuenciales secuencial = new FicherosSecuenciales(nombreArchivo);
		FicherosAccesoAleatorio aleatorio = new FicherosAccesoAleatorio ("fechas");
		
		
		//creacion de menus y sub menus
		int opcion;
		do {
			
			//menu principal con su bucle
		System.out.println("*************** MENU ****************");
		System.out.println("1-Ficheros secuenciales ");
		System.out.println("2-Ficheros de acceso aleatorio");
		System.out.println("3-Salir");
		System.out.println("Introduce una opcion:");
		
		opcion = escaner.nextInt();
		
		switch(opcion) {
		
		//introcuion de datos y  switch del menu principal
		case 1:
			
			int opcion1;
			do {
				
				//sub menu de secuencial  
			System.out.println("*********SUBMENU FICHEROS SECUENCIALES*************");
			System.out.println("1- Crear un fichero por teclado"); /*HECHO*/
			System.out.println("2- Visualizar fichero");  /*HECHO*/
			System.out.println("3- Buscar en el fichero "); /*HECHO*/
			System.out.println("4- Ordenar alfabeticamente (EXTRA) "); /*HECHO*/
			System.out.println("5- Poner mayuscula la primera letra de la palabra (EXTRA)");/*HECHO*/
			System.out.println("6- Mostrar contenido del fichero sin espacios, sin modificar el fichero (EXTRA)");/*HECHO*/
			System.out.println("7- Contar consonantes (EXTRA)");
			System.out.println("8- Numero de lineas de el fichero (EXTRA)"); /*HECHO*/
			System.out.println("9-Salir");
			System.out.println("Introduce una opcion:");
			opcion1= escaner.nextInt();
			
			
			
			switch (opcion1) {
		
				case 1: 
					//creacion de archivo secuencial

					secuencial.crearArchivo();
					
					break;
			
			
				case 2:
					//visualizacion de archivo
					secuencial.visualizarArchivo();
					
					break;
				case 3:
					
					//metodo para buscar en archivo secuencial
					secuencial.buscar();
					
					break;
					
					
				case 4:
					//metodos ordenar alfabeticamente 
					secuencial.ordenarAlfabeticamente();
					secuencial.visualizarArchivo();
			
					break;
					
					
				case 5:
					//poner primera etra de fila en mayuscula
					secuencial.primeraMayus();
					secuencial.visualizarArchivo();
					break;
					
				case 6:
					//mostrar sin espacios sin cambiar en el fichero
					 secuencial.mostarSinEspacios();
					
					break;
					
					
				case 7:
					//numero de consonantes que hay en un fichero
						System.out.println("El numero de consonantes es: " );
						secuencial.contarConosantes();
					
					break;
					
					
				case 8:
					//numero de lineas que hay en el fichero
					System.out.println("El numero de lineas del fichero es: "+secuencial.numeroLineas());
					break;
					
					
				case 9:
					
					//salir
					System.out.println("Has selcionado la opcion de salir de Ficheros Secuenciales");
				
					break;
			
			}
			
			
			//fin segundo bucle
			}while(opcion1!=9);
			
		break;
		
		
		case 2:
			
			//submenu 2 aleatorios y bucle
			int opcion2;
			do {
			System.out.println("*********SUBMENU FICHEROS DE ACCESO ALEATORIO*************");
			System.out.println("1- Escribrir en un fichero aleatorio ");
			System.out.println("2- Visualizar fichero aleatorio");
			System.out.println("3- Modificar fichero aleatorio");
			System.out.println("4- coprobar si hay compra de entradas en el mes introducido (EXTRA)");
			System.out.println("5- Pasar datos a mayusculas (EXTRA)");
			System.out.println("6- Contar vocales de  fichero aleatorio (EXTRA)");
			System.out.println("7- Pasar datos a minusculas (EXTRA)");
			System.out.println("8- Contar lineas del fichero (EXTRA)");
			System.out.println("9- Salir");
			
			opcion2 = escaner.nextInt();
			
			switch(opcion2){
				
			case 1:
				
				// rellenar archivo
				aleatorio.rellenarArchivo();
				
				
				break;
				
				
			case 2:
				
				//visualizar
				
				aleatorio.visualizarArchivo();
				
				break;
				
				
			case 3:
				//modificar fichero
				aleatorio.modificarArchivo();
				aleatorio.visualizarArchivo();
				
				
				break;
				
			case 4:
				 
				//comprobar mes  si esta
				aleatorio.comprobarMes();
				
				break;
				
				
			case 5:
				//convertir datos del fichero en mayuscula
				aleatorio.convertirMayus();
				aleatorio.visualizarArchivo();
			
				break;
				
			case 6:
				//contar numero de vocales en el fichero
				aleatorio.contarVocales();
			
				break;
				
				
			case 7:
				
				//convertir en minuscula
			
				aleatorio.convertirMinusculas();
				aleatorio.visualizarArchivo();
				break;
				
				
				
			case 8:
				//contar numero de filas
				 System.out.println("El numero de filas que hay es: " +aleatorio.contadorFechas());
				
				break;
				
				
			case 9:
				
				//salir
				
				System.out.println("Has selecionado la opcion de salir de menu de Acceso Aleatorios");
				
				
				break;
				
			}
			
			
			//fin bucle 3
			}while(opcion2!=10);
		break;
		
		
		
		
		case 3:
			
			//caso 3 salir menu principal
			System.out.println("Has selecionado la opcion de salir");
			System.exit(0);
		break;
		
		
		//deafault 
		default :
		System.out.println("Has introducido una opcion invalida");
		
		}
		
		
		//fin blucle principal
		
		}while(opcion!=3);
		

	}

}
